package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.endpoint.DataEndpoint;

@Component
public class DataJsonLoadJaxBCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-load-json-j";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Load data from JSON by JaxB.";
    }

    public void execute() {
        dataEndpoint.loadDataJsonJaxB(getSession());
    }

}