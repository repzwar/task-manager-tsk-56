package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.ProjectDto;
import ru.pisarev.tm.endpoint.ProjectEndpoint;

import java.util.List;

@Component
public class ProjectListShowCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        @Nullable List<ProjectDto> projects = projectEndpoint.findProjectAll(getSession());
        int index = 1;
        for (@NotNull final ProjectDto project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
    }

}
