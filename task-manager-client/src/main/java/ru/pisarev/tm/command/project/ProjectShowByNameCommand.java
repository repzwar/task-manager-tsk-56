package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.ProjectDto;
import ru.pisarev.tm.endpoint.ProjectEndpoint;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class ProjectShowByNameCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = projectEndpoint.findProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }

}
