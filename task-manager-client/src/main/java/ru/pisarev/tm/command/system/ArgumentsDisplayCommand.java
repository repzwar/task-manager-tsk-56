package ru.pisarev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.service.CommandService;

@Component
public class ArgumentsDisplayCommand extends AbstractCommand {


    @NotNull
    @Autowired
    private CommandService commandService;

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        int index = 1;
        for (@NotNull final String arg : commandService.getListCommandArg()) {
            System.out.println(index + ". " + arg);
            index++;
        }
    }

}
