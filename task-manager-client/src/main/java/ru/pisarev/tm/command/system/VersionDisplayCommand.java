package ru.pisarev.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.service.PropertyService;

@Component
public class VersionDisplayCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
