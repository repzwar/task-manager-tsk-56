package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskFindAllTaskByProjectIdCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-find-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in project by project id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final List<TaskDto> tasks = taskEndpoint.findTaskByProjectId(getSession(), id);
        System.out.println("TaskDto list for project");
        for (@NotNull TaskDto task : tasks) {
            System.out.println(task.toString());
        }
    }

}
