package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskShowByIdCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskDto task = taskEndpoint.findTaskById(getSession(), id);
        if (task == null) throw new TaskNotFoundException();
        show(task);
    }

}
