package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public class TaskShowListCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable List<TaskDto> tasks = taskEndpoint.findTaskAll(getSession());
        int index = 1;
        for (@NotNull TaskDto task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
    }

}
