package ru.pisarev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.endpoint.AdminEndpoint;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class UserUnlockByLoginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        adminEndpoint.unlockByLogin(getSession(), login);
    }

}
