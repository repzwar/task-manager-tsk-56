package ru.pisarev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.exception.system.UnknownCommandException;
import ru.pisarev.tm.service.CommandService;
import ru.pisarev.tm.service.LogService;
import ru.pisarev.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    protected FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] commands;

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Autowired
    private LogService logService;

    public void start(String... args) {
        displayWelcome();
        initCommands();
        if (runArgs(args)) System.exit(0);
        process();
    }

    public void initApplication() {
        initPID();
    }

    @SneakyThrows
    private void initCommands() {
        if (commands == null) return;
        Arrays.stream(commands).filter(Objects::nonNull).forEach(t -> commandService.add(t));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        fileScanner.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

}
