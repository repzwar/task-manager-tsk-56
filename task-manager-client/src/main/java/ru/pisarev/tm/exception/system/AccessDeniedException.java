package ru.pisarev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    @NotNull
    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
