package ru.pisarev.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRecordRepository;
import ru.pisarev.tm.dto.SessionRecord;

import java.util.List;

public interface ISessionRecordRepository extends IRecordRepository<SessionRecord> {


    List<SessionRecord> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final SessionRecord session);

}
