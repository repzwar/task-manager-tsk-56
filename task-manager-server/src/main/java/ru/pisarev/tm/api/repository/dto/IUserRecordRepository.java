package ru.pisarev.tm.api.repository.dto;

import ru.pisarev.tm.api.IRecordRepository;
import ru.pisarev.tm.dto.UserRecord;

public interface IUserRecordRepository extends IRecordRepository<UserRecord> {

    UserRecord findByLogin(final String login);

    UserRecord findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final UserRecord user);

}
