package ru.pisarev.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmailExistException extends AbstractException {

    @NotNull
    public EmailExistException() {
        super("Error. Email already exist.");
    }

    @NotNull
    public EmailExistException(String value) {
        super("Error. Email '" + value + "' already exist.");
    }

}
